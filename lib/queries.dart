class Queries {
  static const String addQuery = r'''
    mutation($is_completed: Boolean!, $title: String!) {
      createTodo(input: {completed: $is_completed, title: $title}) {
          id
          title
          completed
          }
    }
  ''';

  static const String editQuery = r'''
    mutation update_todo($id: ID!, $is_completed: Boolean, $title: String) {
      updateTodo(input: {title: $title, completed: $is_completed}, id: $id) {
        id,
        title,
        completed,
      }
    }
  ''';

  static const String deleteQuery = r"""
  mutation delete_todo($id: ID!){
    deleteTodo(id: $id)
}
""";

  static const String getAllTodosAscQuery = r"""
    query {
      todos{
      data{
        id,
        title,
        completed,
      }
    }
  } """;
}
