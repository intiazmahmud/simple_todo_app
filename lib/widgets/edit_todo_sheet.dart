import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:simple_todo_app/models/todo_model.dart';
import 'package:simple_todo_app/project_utils.dart';

class EditTodoBottomSheet extends StatefulWidget {
  final TodoModel todoModel;
  final Future<bool> Function(TodoModel todoModel) editTodoCallback;
  final Future<bool> Function(TodoModel todoModel) deleteTodoCallback;

  const EditTodoBottomSheet({
    super.key,
    required this.todoModel,
    required this.editTodoCallback,
    required this.deleteTodoCallback,
  });

  @override
  State<EditTodoBottomSheet> createState() => _EditTodoBottomSheetState();
}

class _EditTodoBottomSheetState extends State<EditTodoBottomSheet> {
  void _showToast(BuildContext context, String message) =>
      fToast.showToast(child: ProjectUtils.toastWidget(context, message));

  late FToast fToast;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController todoController = TextEditingController();
  int _priorityValue = 1;
  bool _isLoading = false;

  @override
  void initState() {
    todoController.text = widget.todoModel.title;
    _priorityValue = widget.todoModel.priority;
    fToast = FToast();
    fToast.init(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.only(
          left: 32,
          right: 32,
          top: 16,
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Wrap(
              // mainAxisSize: MainAxisSize.min,
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Edit Todo',
                  style: TextStyle(
                    fontSize: 26.sp,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(height: 16.h),
                TextFormField(
                  controller: todoController,
                  decoration: const InputDecoration(
                    isDense: true,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(16)),
                    ),
                    hintText: 'Edit todo',
                  ),
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.h),
                if (_isLoading)
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16),
                    child: ProjectUtils.circularProgressBar(context),
                  ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: Row(
                    children: [
                      Expanded(
                        child: ElevatedButton.icon(
                          onPressed: () async {
                            await _editTodoOnPressed(context);
                          },
                          label: const Text('Save'),
                          icon: const Icon(Icons.edit),
                        ),
                      ),
                      SizedBox(width: 32.w),
                      Expanded(
                        child: ElevatedButton.icon(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.red),
                          ),
                          onPressed: () async {
                            await _deleteTodoOnPressed(context);
                          },
                          label: const Text('Delete'),
                          icon: const Icon(Icons.delete),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _editTodoOnPressed(BuildContext context) async {
    if (!_formKey.currentState!.validate()) {
      return;
    }
    setState(() {
      _isLoading = true;
    });
    widget.todoModel.priority = _priorityValue;
    widget.todoModel.title = todoController.text;

    bool response = await widget.editTodoCallback(widget.todoModel);
    if (response && mounted) {
      Navigator.pop(context);
      _showToast(context, 'EDITED!');
    } else {
      _showToast(context, 'Error while saving the edited todo.');
      setState(() {
        _isLoading = false;
      });
    }
  }

  Future<void> _deleteTodoOnPressed(BuildContext context) async {
    setState(() {
      _isLoading = true;
    });
    bool response = await widget.deleteTodoCallback(widget.todoModel);
    if (response && mounted) {
      Navigator.pop(context);
      _showToast(context, 'DELETED!');
    } else {
      _showToast(context, 'Error while deleting todo.');
      setState(() {
        _isLoading = false;
      });
    }
  }
}
