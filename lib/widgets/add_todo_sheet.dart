import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:simple_todo_app/models/todo_model.dart';
import 'package:simple_todo_app/project_utils.dart';

class AddTodoBottomSheet extends StatefulWidget {
  final Future<bool> Function(TodoModel todoModel) addTodoCallback;

  const AddTodoBottomSheet({super.key, required this.addTodoCallback});

  @override
  State<AddTodoBottomSheet> createState() => _AddTodoBottomSheetState();
}

class _AddTodoBottomSheetState extends State<AddTodoBottomSheet> {
  void showSnackBarMessage(String message) {
    if (mounted) ProjectUtils.showSnackBarMessage(context, message);
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController todoController = TextEditingController();
  int _priorityValue = 1;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 32,
        right: 32,
        top: 16,
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Add Todo',
              style: TextStyle(
                fontSize: 26.sp,
                fontWeight: FontWeight.w400,
              ),
            ),
            SizedBox(height: 16.h),
            TextFormField(
              controller: todoController,
              decoration: const InputDecoration(
                isDense: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                ),
                hintText: 'Enter todo',
              ),
              validator: (String? value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
            ),
            SizedBox(height: 32.h),
            if (_isLoading) ProjectUtils.circularProgressBar(context),
            Align(
              alignment: Alignment.bottomRight,
              child: FloatingActionButton(
                child: const Icon(Icons.save),
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    String todoText = todoController.text;
                    int todoPriority = _priorityValue;

                    TodoModel model = TodoModel(
                      id: -1,
                      createdAt: DateTime.now(),
                      title: todoText,
                      isCompleted: false,
                      priority: todoPriority,
                    );

                    setState(() {
                      _isLoading = true;
                    });
                    bool result = await widget.addTodoCallback(model);
                    if (!result) {
                      setState(() {
                        _isLoading = false;
                      });
                    }
                  }
                },
              ),
            ),
            SizedBox(height: 16.h),
          ],
        ),
      ),
    );
  }
}
